//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses');
	const usersRoutes = require('./routes/users');
	
//[SECTION] Environment Variables Setup
	dotenv.config();
	//<Configure Variables Here>
	const port = process.env.PORT;
	const mongodb = process.env.MONGO_DB;

//[SECTION] Server Setup
	const app = express();
	app.use(cors());
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

//[SECTION] Database Connect
	mongoose.connect(mongodb);
	let db = mongoose.connection;

	db.once("open", () => {
		console.log('You are now connected to MongoDb Atlas')
	})
	

//[SECTION]Server Routes
	app.use('/courses', courseRoutes);
	app.use('/users', usersRoutes);


//[SECTION] Server Responses
	app.get('/', (req,res) =>{
		res.send(`Project Deployed Succcessfully`);
	});

	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});
