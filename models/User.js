//[SECTION]DEPENDENCIES and Modules
	const mongoose = require('mongoose');

//[SECTION]Schema for User
	
	const userBlueprint = new mongoose.Schema({
		firstName: {
			type : String,
			required : [true, 'First name is required']
		},
		lastName: {
			type : String,
			required : [true, 'Last name is required']
		},
		email: {
			type : String,
			required : [true, 'Email is required']
		},
		password: {
			type : String,
			required : [true, 'Password is required']
		},
		isAdmin: {
			type : Boolean,
			default : false
		},
			mobileNo: {
			type : String,
			required : [true, 'Mobile no. is required']
		},
		enrollment: [
			{
				courseId: {
					type:String,
					required: [true, 'Course Id is required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'enrolled'
				}
			}
		] 
	});


//[SECTION] Schema  EXPOSE
 	module.exports = mongoose.model("User", userBlueprint);
