//[SECTION]Dependencies and Modules
	const User = require('../models/User');
	const Course = require('../models/Course');
	const bcrypt = require('bcrypt')
	const auth = require('../auth')

//[SECTION]Functionality [Create] 
	//Register User
	module.exports.registerUser = (data) => {
	let fName = data.firstName;
	let lName = data.lastName;
	let email = data.email;
	let pass  = data.password;
	let mobNo = data.mobileNo;	
	let newUser = new User ({
		firstName: fName,
		lastName: lName,
		email: email,
		password: bcrypt.hashSync(pass, 10),
		mobileNo: mobNo
	});
	return	newUser.save().then((createdUser, Err) => {
		if (createdUser) {
			return createdUser
		} else {
			return false
		}
	});
	};

	//Check Email
	module.exports.checkEmailExist = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email Already Exists';
			} else {
				return 'Email is still available';
			}
			})
		};
	
	//Login (User Authentication)
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email
		let pass = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
					return false;
			} else {
			let passW = result.password; //colection
					const isMatched = bcrypt.compareSync(pass, passW);
					if (isMatched) {
						let dataNiUser = result.toObject()
						return{access: auth.createAccessToken(dataNiUser)};
					} else {
						return false
					}
			};
		});
	};
	
	//Enroll (user)
	module.exports.enrollUser = async (data) => {
			let id = data.userId
			let courseId = data.courseId;			
			
			let isCourseUpdated = await Course.findById(courseId).then(course => {
			const userFound = course.enrollees.find(courseObj => courseObj.userId == id);
				if (!userFound) {
					course.enrollees.push({userId: id});					
					return course.save().then((save, err) => {
						if (err) {	
							return false;
						} else {
							return true;
						}
					});
				} else {
					return false;
				}
			});

			let isUpdatedUser = await User.findById(id).then(user => {
				const courseFound = user.enrollment.find(courseObj => courseObj.courseId == courseId);
				if (!courseFound) {
					user.enrollment.push({courseId: courseId});
					return user.save().then((save, err) =>{
						if (err) {
							return false;
						} else {
							return true;
						}
					});					
				} else {
					return false;
				}
			});

		if (isUpdatedUser  && isCourseUpdated) {
			return `enrollment sucess`
		} else {
			return 'Course Exist' 
		}

	};

//[SECTION]Functionality [Retrieve]
	module.exports.getProfile = (id) => {
			return User.findById(id).then(user =>{
				return user;
			})
	}

//[SECTION]Functionality [Update]
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin : true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return `Updates Failed to implement`
			}
		});
	};
	
	//Set as non admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin : false
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return `Updates Failed to implement`
			}
		});
	};



//[SECTION]Functionality [Delete]
