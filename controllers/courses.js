//[SECTION]Dependencies and Modules
	const Course = require('../models/Course');


//[SECTION] Functionality  [Create]
	module.exports.addCourse = (info) => {
		let course = info.course
			let cName = course.name;		
			let cDesc = course.description;		
			let cPrice = course.price;
		
			let newCourse = new Course ({
				name: cName,
				description: cDesc,
				price: cPrice
			
				});
			return newCourse.save().then((createdCourse, Err) => {
				if (createdCourse) {
					return createdCourse;
				} else {
					return false;
				};
			});
	};

//[SECTION] Functionality  [Retrieve]
	module.exports.getAllCourse = () => {
		return Course.find({}).then(result => {
			return result;
		});
	};

	//Retrieve ONLY Active Course (Regular User)
 module.exports.getAllActive = () => {
   		return Course.find({isActive: true}).then(result => {
   			return result;
   		});
   };


	//Retrieve Single Course
	module.exports.getCourse = (id) => {
		return Course.findById(id).then((success, error) =>{
			if (success) {
				return success
			} else {
				return 'No Course Found!'
			};
		});
	};

//[SECTION] Functionality  [Update]
	module.exports.updateCourse = (course, details) => {
		let  cName = details.name;		
		let  cDesc = details.description;		
		let  cCost = details.price;
		let updatedCourse =  {	
			name: cName,
			description: cDesc,
			price: cCost
		 };
		 let id = course.courseId;	
		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) =>{
			if (courseUpdated) {
				return 'Update course successfully';
			} else {
				return 'Failed to update Course'
			};
		});
	};
	//Archive Course
	module.exports.archiveCourse = (course) => {
			let id = course.courseId;
			let updates = {
				isActive: false
			}
		return Course.findByIdAndUpdate(id, updates).then((archived, err)=>{
			if (archived) {
				return `successfully archived course ID:${id}`
			} else {
				return false;
			};
		});	

	};

	//Delete Course ALTERNATE APPROACT
	module.exports.deleteCourse = (courseId) =>{
			return Course.findByIdAndRemove(courseId).then((removedCourse, err)=>{
				if (removedCourse) {
					return `successfully remove ${removedCourse}`
				} else {
					return `No Course Found!`
				}
			});
	};