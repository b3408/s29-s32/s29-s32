//[SECTION]Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/courses');
	const auth = require('../auth');

//[SECTION]Routing Component
	const route = express.Router();

//[SECTION]-[POST] Route [ADMIN]
	route.post('/', auth.verify ,(req,res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin;
		let data = {
			course: req.body,
		}
		if (isAdmin) {
				controller.addCourse(data).then(outcome => {
			res.send(outcome)});
		} else {
			res.send('user unauthorized to proceed')
		};
	});
	
//[SECTION]-[GET] Route [ADMIN]
	//RETRIEVE ALL COURSES [ADMIN]
		route.get('/all', auth.verify,(req,res) => {
			let token = req.headers.authorization;
			let payload = auth.decode(token)
			let isAdmin = payload.isAdmin
			isAdmin ? controller.getAllCourse().then(outcome => {
				res.send(outcome);
			})
			: res.send('unauthorized access')
	});

	//RETRIEVE ACTIVE COURSES [USER] 
		route.get('/', (req,res) => {
			controller.getAllActive().then(outcome => {
				res.send(outcome)
			});
		});

	//GET SINGLE COURSE
		route.get('/:id', (req,res) => {
			let data = req.params.id
			controller.getCourse(data).then(result => {
				res.send(result);
			});
		
		});

//[SECTION]-[PUT] Route
	//Update Course [ADMIN]
	route.put('/:courseId', auth.verify,(req,res) => {
		let params = req.params;
		let body = req.body;
		if (!auth.decode(req.headers.authorization).isAdmin) {
			res.send(`User  unauthorized`)
		} else {
			controller.updateCourse(params, body).then(outcome => {
			res.send(outcome);
		});
		};
	});

	//Archive Route [ADMIN]
	route.put('/:courseId/archive', auth.verify,(req,res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let params = req.params;
		(isAdmin)?
		   controller.archiveCourse(params).then(result => {
			res.send(result);
		})
		:
			res.send('unauthorized user')
	});

//[SECTION]-[DEL] Route [ADMIN]
	route.delete('/:courseId/delete', auth.verify,(req,res) =>{
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin
		let id = req.params.courseId
		 isAdmin ?
		controller.deleteCourse(id).then(result =>{
			res.send(result);
		})
		: res.send('unauthorized user')
	});

//[SECTION]Export Route System
module.exports = route;