//[SECTION]Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth')

//[SECTION]Routing Componnet
	const route = express.Router();

//[SECTION] Routes - [POST]
	route.post('/register', (req,res) =>{
		 let userData = req.body
		controller.registerUser(userData).then(outcome =>{
			res.send(outcome)
		});
	
	});

	route.post('/checkemail', (req,res) => {
		controller.checkEmailExist(req.body).then(result =>{
			res.send(result);
		});
	});

	//Login [User] 
	route.post('/login', (req,res) => {
			let data = req.body
			controller.loginUser(data).then(result =>{
				res.send(result)
			});
	});	

	//Auethenticated User Course Enrollment
	route.post('/enroll', auth.verify,(req,res) =>{
		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let userId = payload.id //user id
		let isAdmin = payload.isAdmin; 
		
		let subjectId = req.body.courseId; 
		let data = {
			userId: userId,
			courseId: subjectId
		};
		if (!isAdmin) {
			//permit 
			//
			controller.enrollUser(data).then(outcome =>{
				res.send(outcome);
			}) 
		} else {
			res.send('Dont allow to enroll')
		};
	})



//[SECTION] Routes - [GET]
	route.get('/details', auth.verify,(req,res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(result =>{
			res.send(result);
		});
	});

//[SECTION] Routes - [UPDATE]

	route.put('/:userId/set-as-admin', auth.verify,(req,res) =>{
			let token = req.headers.authorization;
			let payload = auth.decode(token);
			let isAdmin = payload.isAdmin
			let id = req.params.userId;
		(isAdmin) ?controller.setAsAdmin(id).then(result => res.send(result)): res.send('unauthorized')
	});

		//set user as non admin
	route.put('/:userId/set-as-user', auth.verify ,(req, res) => {
   	   let token = req.headers.authorization;
   	   let isAdmin = auth.decode(token).isAdmin;
   	   let id = req.params.userId;
   	   isAdmin ? controller.setAsNonAdmin(id).then(result => res.send(result)) 
   	   : res.send('Unauthorized User');  	   
   });


//[SECTION] Routes - [DELETE]



//[SECTION]Expose Route System
module.exports = route;